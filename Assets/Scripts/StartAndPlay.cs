﻿using System;
using System.IO;
using FMOD;
using FMOD.Studio;
using FMODUnity;
using UnityEngine;
using UnityEngine.UI;
using INITFLAGS = FMOD.Studio.INITFLAGS;

public class StartAndPlay : MonoBehaviour
{
    [SerializeField] private Text label;

    private FMOD.Studio.System fmodSystem;

    // Start is called before the first frame update
    void Start()
    {
        Log("starting");
        RuntimeUtils.EnforceLibraryOrder();
        CreateSystem(out fmodSystem);
        InitializeSystem(fmodSystem);

        FMOD.System coreSystem;
        GetCore(out coreSystem);

        PrintAllDrivers(coreSystem);

        LoadBank(fmodSystem, "Starter.strings.bank");
        LoadBank(fmodSystem, "Starter.bank");
        EventDescription evtDescription;
        GetEventDescription(fmodSystem, out evtDescription);
        EventInstance evtInstance;
        CreateInstance(evtDescription, out evtInstance);
        StartInstance(evtInstance);
    }

    private void PrintAllDrivers(FMOD.System coreSystem)
    {
        int numDrivers;
        var result = coreSystem.getNumDrivers(out numDrivers);
        if (result != RESULT.OK)
        {
            Log("Failed getNumDrivers. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }

        for (int i = 0; i < numDrivers; i++)
        {
            string nameDriver;
            Guid guid;
            int systemrate;
            SPEAKERMODE speakermode;
            int speakerChannels;
            result = coreSystem.getDriverInfo(i,
                out nameDriver,
                1000,
                out guid,
                out systemrate,
                out speakermode,
                out speakerChannels);
            
            if (result != RESULT.OK)
            {
                Log("Failed getDriverInfo. "+ result.ToString() + " - " + i);
            }
            else
            {
                Log(string.Format(
                    "nameDriver {0}; guid {1}; systemrate {2}; speakermode {3}; speakerChannels {4}",
                    nameDriver,
                    guid,
                    systemrate,
                    speakermode,
                    speakerChannels));
            }
        }
    }

    private void GetCore(out FMOD.System coreSystem)
    {
        var result = fmodSystem.getCoreSystem(out coreSystem);
        if (result != RESULT.OK)
        {
            Log("Failed GetCoreSystem. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }
    }

    private void OnDisable()
    {
        fmodSystem.unloadAll();
        fmodSystem.release();
    }

    private void Update()
    {
        fmodSystem.update();
    }

    private void Log(string log)
    {
        label.text = log;
        UnityEngine.Debug.Log(log);
    }
    
    private void StartInstance(EventInstance instance)
    {
        Log("StartInstance. ");
        var result = instance.start();
        if (result != RESULT.OK)
        {
            Log("Failed StartInstance. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }
    }
    
    private void CreateInstance(FMOD.Studio.EventDescription evtDescription, out EventInstance instance)
    {
        Log("CreateInstance. ");
        var result = evtDescription.createInstance(out instance);
        if (result != RESULT.OK)
        {
            Log("Failed CreateInstance. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }
    }

    private void GetEventDescription(FMOD.Studio.System system, out EventDescription evtDescription)
    {
        Log("GetEventDescription. ");
        var result =  system.getEventByID(new Guid("65c7fb39-8f8b-4aff-beb2-a5ab49fa1207"), out evtDescription);
        if (result != RESULT.OK)
        {
            Log("Failed GetEvent Description. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }
    }

    private void LoadBank(FMOD.Studio.System system, string starterStringsBank)
    {
        Log("LoadBank. ");
        FMOD.Studio.Bank bank;
        var result = system.loadBankFile(
            Path.Combine(Application.streamingAssetsPath, starterStringsBank),
            LOAD_BANK_FLAGS.NORMAL,
            out bank);
        if (result != RESULT.OK)
        {
            Log("Failed LoadBank. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }
    }

    private void CreateSystem(out FMOD.Studio.System system)
    {
        Log("CreateSystem. ");
        var result = FMOD.Studio.System.create(out system);
        if (result != RESULT.OK)
        {
            Log("Failed CreateSystem. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }
    }
    
    private void InitializeSystem(FMOD.Studio.System system)
    {
        Log("InitializeSystem. ");
        var result = system.initialize(
            32,
            INITFLAGS.NORMAL, 
            FMOD.INITFLAGS.NORMAL, 
            IntPtr.Zero);
        
        if (result != RESULT.OK)
        {
            Log("Failed InitializeSystem. "+ result.ToString());
        }
        else
        {
            Log(result.ToString());
        }
    }
}
