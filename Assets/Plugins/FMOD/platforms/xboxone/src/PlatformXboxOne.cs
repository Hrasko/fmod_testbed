﻿using System;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

#if UNITY_XBOXONE && !UNITY_EDITOR
namespace FMOD
{
    public partial class VERSION
    {
        public const string dll = "fmod" + dllSuffix;
    }
}

namespace FMOD.Studio
{
    public partial class STUDIO_VERSION
    {
        public const string dll = "fmodstudio" + dllSuffix;
    }
}
#endif

namespace FMODUnity
{
#if UNITY_EDITOR
    [InitializeOnLoad]
#endif
    public class PlatformXboxOne : Platform
    {
        static PlatformXboxOne()
        {
            Settings.AddPlatformTemplate<PlatformXboxOne>("4dc32f950691bad4da8c90ee65bb0a06");
        }

        public override string DisplayName { get { return "Xbox One"; } }
        public override void DeclareUnityMappings(Settings settings)
        {
            settings.DeclareRuntimePlatform(RuntimePlatform.XboxOne, this);

#if UNITY_EDITOR
            settings.DeclareBuildTarget(BuildTarget.XboxOne, this);
#endif
        }

#if UNITY_EDITOR
        public override Legacy.Platform LegacyIdentifier { get { return Legacy.Platform.XboxOne; } }

        protected override IEnumerable<string> GetRelativeBinaryPaths(BuildTarget buildTarget, string suffix)
        {
            yield return string.Format("xboxone/fmod{0}.dll", suffix);
            yield return string.Format("xboxone/fmodstudio{0}.dll", suffix);
        }
#endif

#if !UNITY_EDITOR
        public override void PreSystemCreate(Action<FMOD.RESULT, string> reportResult)
        {
            RuntimeUtils.SetThreadAffinity(reportResult);
        }
#endif

        public override string GetPluginPath(string pluginName)
        {
            return string.Format("{0}/{1}.dll", GetPluginBasePath(), pluginName);
        }
#if UNITY_EDITOR
        public override OutputType[] ValidOutputTypes
        {
            get
            {
                return sValidOutputTypes;
            }
        }

        private static OutputType[] sValidOutputTypes = {
           new OutputType() { displayName = "Windows Audio Session API", outputType = FMOD.OUTPUTTYPE.WASAPI },
           new OutputType() { displayName = "Windows Sonic", outputType = FMOD.OUTPUTTYPE.WINSONIC },
        };
#endif
    }
}
